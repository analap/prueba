class Empleado:
    def __init__(self, nombre, salario, tasa, antigüedad):
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__antigüedad = antigüedad
    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre,tax=self.__impuestos))
        return self.__impuestos
    def ImprimirAntigüedad(self):
        print("Edad de " + self.__nombre + ":" + str(self.__antigüedad))
    def AhorroImpuestos():


def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))

emp1 = Empleado("Pepe", 20000, 0.35, 50)
print(emp1.nombre)
emp1.ImprimirAntigüedad
emp2 = Empleado("Ana", 30000, 0.30, 48)

empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 20), Empleado("Luisa", 25000, 0.15, 30)]
total = 0
for emp in empleados:
    emp.ImprimirAntigüedad
    total += emp.CalculoImpuestos()
displayCost(total)